"""Segmentation Module for airsim

"""
import logging

logger = logging.getLogger("AirSimCapture")
logger.setLevel(logging.DEBUG)

# airsim.wait_key('Press any key to set all object IDs to 0')
# found = client.simSetSegmentationObjectID("[\w*. ]*", 0, True);
# print("Done: %r" % (found))

REGEX_CATCH_ALL = "[\w*. ]*"

def set_all_to_zero(client, code=0):
    found = client.simSetSegmentationObjectID(REGEX_CATCH_ALL, code, True)
    if not found:
        logger.warning(
            "Segmentation - Could not find %s in Unreal Environment to set to code %r", REGEX_CATCH_ALL, code)


def set_segmentation_ids(client, regex_codes):
    for regex_str, code in regex_codes:
        found = client.simSetSegmentationObjectID(regex_str, code, True)
        if not found:
            logger.warning(
                "Segmentation - Could not find %s in Unreal Environment to set to code %r", regex_str, code)
