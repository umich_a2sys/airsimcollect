import sys
import logging
import asyncio
import time
import math
from os.path import join
import json

import numpy as np
import airsim
from airsim import Vector3r, to_quaternion
from airsimcollect import AirSimCollect

from airsimcollect import parse_lidarData

from airsimcollect.helper import update, update_collectors, DEFAULT_CONFIG

DEG_TO_RAD = math.pi / 180.0

logger = logging.getLogger("AirSimCapture")

MODE = 1 # 0 - FPV, Drone Looking Down
# if 0 - settings.json = "ViewMode": "Fpv"
# if 0 - settings.json = "ViewMode": "FlyWithMe", DebugPoints?

SPEED = 5

def to_ned(path_unreal):
    path_ned = path_unreal - origin
    path_ned[:,2] = - path_ned[:,2]
    path_ned = path_ned / 100
    return path_ned

origin = np.array([1490, -1120, 2590])
path_unreal_video = np.array([
    [-692, -6482, 2480 ], 
    [360, 1980, 2480 ], 
    [1570, 2840, 2480 ],
    [2620, 2840, 2580 ],
    [2780, 3710, 2590 ],
    [2780, 4870, 2510 ],
    [1790, 5430, 2450 ], # AIR VENT
    [250, 5990, 2680 ],
])

path_unreal_drone = np.array([
    # [1790, 5430, 2450 ], # Middle Street
    [-890, -3850, 4540 ], # Top Building
    [-890, -5110, 5230 ], # Top Building
])

path_unreal_landing_approach = np.array([
    [-890, -7190, 5400 ], # Top Building
])

path_unreal_landing = np.array([
    [-890, -7550, 4960],
    [-740, -8270, 4600],
    [-740, -9140, 4300],
])


if MODE == 0:
    PITCH = 0.0 * DEG_TO_RAD
    paths = [to_ned(path_unreal_video)]
    yaw_changes = [{"pre": 90, "post": -90}, None]
    speed_changes = [SPEED, SPEED]
    pause_times = [1, 5]
else:
    PITCH = -40.0 * DEG_TO_RAD
    paths = [to_ned(path_unreal_video), to_ned(path_unreal_drone), to_ned(path_unreal_landing_approach)]
    yaw_changes = [{"pre": 90, "post": -90}, None, None]
    speed_changes = [SPEED, SPEED, 2]
    pause_times = [1, 1, 2]



class Cinema(AirSimCollect):
    def __init__(self, paths, yaw_changes, speeds, pauses, *args, **kwargs):
        super(Cinema, self).__init__(*args, **kwargs)
        self.paths = paths
        self.yaw_changes = yaw_changes
        self.speeds = speeds
        self.pauses = pauses
        self.connect_airsim_cinema()

    def connect_airsim_cinema(self):
        try:
            self.client.enableApiControl(True)
            self.client.armDisarm(True)
            self.client.simSetCameraOrientation('0', to_quaternion(PITCH, 0.0, 0.0))
            return True
        except Exception as e:
            logger.exception(
                "Can not connect to AirSim vehicle! Is AirSim running? Exiting early")
            sys.exit()
    def take_off(self):
        landed = self.client.getMultirotorState().landed_state
        if landed == airsim.LandedState.Landed:
            print("Taking off...")
            self.client.takeoffAsync().join()
        else:
            print("Hovering...")
            self.client.hoverAsync().join()
        print("Finished taking off..")

    def execute_path(self):
        for i, (path, speed, yaw, pause) in enumerate(zip(self.paths, self.speeds, self.yaw_changes, self.pauses)):
            airsim_path = []
            # Pre yaw rotation for path
            if yaw is not None:
                yaw_amt = yaw.get("pre", 0.0)
                print("Performing Yaw Change")
                self.client.rotateToYawAsync(yaw_amt, 2.0).join()
                if yaw_amt > 1:
                    time.sleep(1)
            for i in range(path.shape[0]):
                airsim_path.append(Vector3r(path[i, 0], path[i, 1], path[i, 2]))
            # print(airsim_path)
            print("Starting Path Execution...")
            self.client.moveOnPathAsync(airsim_path, speed, timeout_sec=100).join()
            print("Finished Path Execution")
            self.client.hoverAsync().join()
            # Post yaw rotation for path
            if yaw is not None:
                yaw_amt = yaw.get("post", 0.0)
                print("Performing Yaw Change")
                self.client.rotateToYawAsync(yaw_amt, 2.0).join()
            time.sleep(pause)
            # record data

    def execute_landing_approach(self, path_unreal):
        # Save Data
        record = self.collect_data_at_point(1.0, 2.0)
        self.save_records([record])
        print("Executing Landing Approach")
        airsim_path = []
        path = to_ned(path_unreal)
        for i in range(path.shape[0]):
            airsim_path.append(Vector3r(path[i, 0], path[i, 1], path[i, 2]))
        self.client.moveOnPathAsync(airsim_path, 2, timeout_sec=100).join()
        self.client.hoverAsync().join()

    def execute_landing(self):
        print("Performing Landing")
        self.client.landAsync().join()
        self.client.enableApiControl(False)

def main():
    with open("assets/rooftop/config_cinema.json") as file:
        config = json.load(file)
    config = update(DEFAULT_CONFIG, config)
    config['collectors'] = update_collectors(config['collectors'])

    cineman_obj = Cinema(paths, yaw_changes, speed_changes, pause_times, **config)
    cineman_obj.take_off()
    cineman_obj.execute_path()
    if MODE == 1:
        cineman_obj.execute_landing_approach(path_unreal_landing)
        cineman_obj.execute_landing()
    time.sleep(1)
    # cineman_obj.client.enableApiControl(False)

if __name__ == "__main__":
    # await main()
    main()
# path.append(airsim.Vector3r(x, self.boxsize, z))