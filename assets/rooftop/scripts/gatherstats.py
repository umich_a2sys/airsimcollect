"""This script will gather all statistics needed for the paper
The script works by analyzing a directory that has been recorded by airsimcollect. It requires Lidar, Scene, and Segmentation data.
A configuration file is passed in that informs the script which commands to run.
It can run the following commands:
    segmentation, classify_point_clouds, polylidar, and polylabel, projectpolygon

Each command will record the follwowing: 
 * 'command' - Command name
 * 'environment' - What environment was this run on, laptop or jetson
 * 'tag' - Any tag to distinguish this record? Usually 'groundtruth' or 'predicted' to signify if we are using a ground truth segmented image
 * 'uid' - Unique id of the record. This is the record id that airsimcollect provides.
 * 'building' - The building name that this record was taken for
 * 'time' - How long did it take for the command to run
 * 'metric' - Some sort of metric for the command, default None
 * 'misc' - Any miscellaneous data to store
These records are saved in the same airsimcollect folder specified as stat_records.json.  We anticipate that
it will be turned into a datframe and analyzed.

"""


import argparse
import sys
from collections import namedtuple
import time
from os import path, makedirs
import json
import math
import logging
import traceback

from polylidar import extractPolygons
from polylabelfast import polylabelfast
from lidarsegmentation.helpers import get_seg2rgb_map, classify_points, project_ned_points
from airsimcollect.helper import import_world
from airsim.types import Quaternionr, Vector3r
from descartes import PolygonPatch
import matplotlib.pyplot as plt
from PIL import Image
from shapely_geojson import Feature, dump
from shapely.geometry import Polygon, shape, Point
import numpy as np

logging.basicConfig(level=logging.INFO)


# C++ Python Modules
record_fields = ['command', 'environment', 'tag', 'uid', 'building', 'time', 'metric', 'misc']
StatRecord = namedtuple('Record', record_fields)

# Set random seed, for noise in lidar point cloud
np.random.seed(1)

NOISE_LEVEL_POLYLIDAR = 0.02


def tuple_to_list(tuplelist):
    return [list(row) for row in list(tuplelist)]


def poly_to_rings(poly):
    ext_coord = tuple_to_list(list(poly.exterior.coords))
    holes = [tuple_to_list(ring.coords) for ring in poly.interiors]
    holes.insert(0, ext_coord)
    return holes


def plot_polygon(polygon, ax, shell_color='green', hole_color='orange'):
    outline = Polygon(shell=polygon.exterior.coords)
    outlinePatch = PolygonPatch(outline, ec=shell_color, fill=False, linewidth=2)
    ax.add_patch(outlinePatch)

    for hole_poly in polygon.interiors:
        outline = Polygon(shell=hole_poly)
        outlinePatch = PolygonPatch(outline, ec=hole_color, fill=False, linewidth=2)
        ax.add_patch(outlinePatch)


def point_radius_to_string(point, radius):
    """Encodes point and radius as a string"""
    return "{:.2f};{:.2f};{:.2f}".format(point[0], point[1], radius)


def get_poly_coords(outline, points, is_3D=False):
    return [get_point(pi, points, is_3D) for pi in outline]


def get_point(pi, points, is_3D=False):
    if is_3D:
        return [points[pi, 0], points[pi, 1], points[pi, 2]]
    else:
        return [points[pi, 0], points[pi, 1]]

# def extract_poly_to_list(poly):
#     """Extract coordinates from a shapely geometry

#     Arguments:
#         poly {shapely.Geometry} -- Shapely Geometry

#     Returns:
#         list -- List of coordinates
#     """

#     coords = [poly.exterior.coords[:]]
#     for interior in list(poly.interiors):
#         coords.append(list(interior.coords))
#     return coords


def transform_points_to_pixel_coords(points, height, img_meta):
    """"Converts NED to pixel coords"""
    ext_coords = np.array(points)
    height_vec = np.ones((ext_coords.shape[0], 1)) * height
    points_z = np.column_stack((ext_coords, height_vec))
    pixels = project_ned_points(points_z, img_meta)
    return pixels


def polygon_to_pixel_coords(polygon, height, img_meta):
    """Converts polygon NED coordinates to their pixel coords. Project points into image"""
    exterior_pixels = transform_points_to_pixel_coords(list(polygon.exterior.coords), height, img_meta)
    holes_pixels = []
    for hole in list(polygon.interiors):
        hole_pixel = transform_points_to_pixel_coords(hole, height, img_meta)
        holes_pixels.append(hole_pixel)

    poly_pixel = Polygon(shell=exterior_pixels, holes=holes_pixels)
    return poly_pixel


def get_best_polygon(polygons, points, building_name, uid):
    """Converts a list of C++ polygon to shapely polygon
    If more than one polygon is returned from polylidar, selects the one with the largest shell
    """
    polygons.sort(key=lambda poly: len(poly.shell), reverse=True)
    if not polygons:
        logging.warn("No polygons returned for uid: %r; building: %r", uid, building_name)
        return None
    if len(polygons) > 1:
        logging.debug("More than one polygon at building %r", building_name)
    poly = polygons[0]
    shell_coords = get_poly_coords(poly.shell, points)
    hold_coords = [get_poly_coords(hole, points) for hole in poly.holes]
    poly_shape = Polygon(shell=shell_coords, holes=hold_coords)
    if not poly_shape.is_valid:
        logging.warn("Invalid polygon for uid: %r; building: %r", uid, building_name)
    # Get height
    z_coords = np.array(get_poly_coords(poly.shell, points, is_3D=True))[:, 2]
    z_mean = np.asscalar(z_coords.mean())
    z_std = z_coords.std()

    poly_feature = Feature(poly_shape, properties={'height': z_mean, 'building': building_name})

    return poly_feature


def timeit(method):
    """Simple timing decorator"""
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        ms = (te - ts) * 1000
        return ms, result
    return timed


def get_base_collect_dir(config):
    return config['collect_config']['save_dir']


def get_lidar_file_name(config, uid):
    base_dir = get_base_collect_dir(config)
    lidar_dir = path.join(base_dir, "Lidar")
    lidar_fname = lidar_dir + '/{}-0.npy'.format(uid)
    lidar_fname = path.realpath(lidar_fname)
    return lidar_fname


def get_polygon_file_name(config, uid, ground_truth):
    base_dir = get_base_collect_dir(config)
    poly_dir = path.join(base_dir, "Polygons")
    suffix = "groundtruth" if ground_truth else "predicted"
    poly_fname = poly_dir + '/{}-0-{}.geojson'.format(uid, suffix)
    poly_fname = path.realpath(poly_fname)
    return poly_fname


def get_classified_lidar_file_name(config, uid, ground_truth):
    base_dir = get_base_collect_dir(config)
    lidar_dir = path.join(base_dir, "LidarClassified")
    suffix = "groundtruth" if ground_truth else "predicted"
    lidar_fname = lidar_dir + '/{}-0-{}.npy'.format(uid, suffix)
    lidar_fname = path.realpath(lidar_fname)
    return lidar_fname


def get_img_file_name(config, uid, img_type='Segmentation', ground_truth=None):
    base_dir = get_base_collect_dir(config)
    if ground_truth is None:
        fname = "{}-0.png".format(uid)
    else:
        suffix = "groundtruth" if ground_truth else "predicted"
        fname = "{}-0-{}.png".format(uid, suffix)
    img_fname = path.join(base_dir, img_type, fname)
    return img_fname


def load_image(config, uid, img_type='Segmentation'):
    """Loads an image as a numpy array from a file"""
    img_fpath = get_img_file_name(config, uid, img_type)
    img = Image.open(img_fpath)
    return np.array(img)


def load_point_cloud(config, uid, classified=False, ground_truth=False):
    """Loads a point cloud from a file"""
    if classified:
        lidar_fname = get_classified_lidar_file_name(config, uid, ground_truth)
    else:
        lidar_fname = get_lidar_file_name(config, uid)
    point_cloud = np.load(lidar_fname)
    return point_cloud


def load_polygon(config, uid, ground_truth=False):
    """Attempts to load a polygon geojson file"""
    poly_fpath = get_polygon_file_name(config, uid, ground_truth)
    try:
        with open(poly_fpath) as f:
            poly_geojson = json.load(f)
        # print(poly_geojson)
        poly = shape(poly_geojson['geometry'])
        # print(poly)
        return poly, poly_geojson['properties']['height']
    except Exception as e:
        return None, None


@timeit
def classify_points_timed(point_cloud, img_meta, img, seg2rgb_map):
    return classify_points(point_cloud, img_meta, img, seg2rgb_map)


def classify_point_cloud(config, uid, img_meta, ground_truth=True):
    """Reads in a point cloud and classified its point using a segmented image"""
    point_cloud = load_point_cloud(config, uid, classified=False)
    img_type = "Segmentation" if ground_truth else "SegmentationPredicted"
    img = load_image(config, uid, img_type)
    seg2rgb_map = config['seg2rgb_map']
    # convert dict to airsim object
    img_meta['rotation'] = Quaternionr(**img_meta['rotation'])
    img_meta['position'] = Vector3r(**img_meta['position'])

    ms, (point_cloud_classified, remove_time) = classify_points_timed(point_cloud, img_meta, img, seg2rgb_map)
    ms = ms - remove_time
    # Save file
    if point_cloud_classified is not None:
        lidar_classified_fname = get_classified_lidar_file_name(config, uid, ground_truth)
        np.save(lidar_classified_fname, point_cloud_classified)
    return ms, point_cloud_classified


def classify_points_clouds(config, ground_truth=True):
    """Classify all record point clouds using segmented image"""
    stat_records = []
    for record in config['records']:
        try:
            uid = record['uid']
            building = record['label']
            environment = config['environment']
            command = 'classify_point_clouds'
            tag = 'groundtruth' if ground_truth else 'predicted'
            img_meta = dict(**record['imgs'][0])
            ms, cpc = classify_point_cloud(config, uid, img_meta, ground_truth=ground_truth)
            metric = None
            misc = cpc.shape[0]
            stat_record = StatRecord(command, environment, tag, uid, building, ms, metric, misc)
            stat_records.append(stat_record)
        except Exception:
            logging.warn("Error! Skipping uid {} for command classify_points_clouds".format(uid))
            # traceback.print_exc(file=sys.stdout)
    return stat_records


@timeit
def extractPolygons_timed(points, **kwargs):
    """Wrapper for polylidar to time execution"""
    polygons = extractPolygons(points, **kwargs)
    return polygons


def polylidar_timed(config, uid, points, ground_truth, building_name, polylidar_kwargs):
    ms, polygons = extractPolygons_timed(points, **polylidar_kwargs)
    feature = get_best_polygon(polygons, points, building_name, uid)
    if feature:
        poly_fpath = get_polygon_file_name(config, uid, ground_truth)
        with open(poly_fpath, "w") as f:
            dump(feature, f, indent=2)

        return ms, feature
    else:
        return None, None


def extract_polygons(config, polylidar_kwargs, ground_truth=True, **kwargs):
    stat_records = []
    for record in config['records']:
        try:
            uid = record['uid']
            building = record['label']
            environment = config['environment']
            command = 'polylidar'
            tag = 'groundtruth' if ground_truth else 'predicted'

            classified_point_cloud = load_point_cloud(config, uid, classified=True, ground_truth=ground_truth)
            # Add 10 cm of noise to prevent coincident points! Only necessary in some situation, but can't be determined before hand successfully yet
            noise = np.random.randn(classified_point_cloud.shape[0], 2) * NOISE_LEVEL_POLYLIDAR
            classified_point_cloud[:, :2] = classified_point_cloud[:, :2] + noise
            logging.debug("Polylidar - UID: %r, Point Cloud Shape: %r", uid, classified_point_cloud.shape)
            ms, _ = polylidar_timed(config, uid, classified_point_cloud, ground_truth, building, polylidar_kwargs)
            metric = None
            misc = ''
            stat_record = StatRecord(command, environment, tag, uid, building, ms, metric, misc)
            stat_records.append(stat_record)
        except Exception as e:
            logging.warn("Error! Skipping uid {} for command polylidar".format(uid))
    return stat_records
    
@timeit
def timed_polylabel(poly_rings, precision=0.5):
    return polylabelfast(poly_rings, precision)


def extract_largest_circle(poly, buffer=-0.5, precision=0.5):
    poly = poly.buffer(buffer)
    poly = poly.simplify(abs(buffer))
    if poly.type == 'MultiPolygon':
        polys = [geom for geom in poly.geoms]
        polys = sorted(polys, key=lambda poly_: poly_.area, reverse=True)
        poly = polys[0]
    poly_list = poly_to_rings(poly)
    # print(poly_list)
    return timed_polylabel(poly_list, precision)


def extract_largest_circles(config, ground_truth=True, **kwargs):
    stat_records = []
    for record in config['records']:
        try:
            uid = record['uid']
            building = record['label']
            environment = config['environment']
            command = 'polylabel'
            tag = 'groundtruth' if ground_truth else 'predicted'
            poly, _ = load_polygon(config, uid, ground_truth=ground_truth)
            metric = None
            ms = None
            misc = None
            if poly:
                ms, (point, radius) = extract_largest_circle(poly, **kwargs)
                # print(ms, point, radius)
                misc = point_radius_to_string(point, radius)

            stat_record = StatRecord(command, environment, tag, uid, building, ms, '', misc)
            stat_records.append(stat_record)
        except Exception as e:
            logging.warn("Error! Skipping uid {} for command polylabel".format(uid))
    return stat_records


def read_records(config):
    base_dir = get_base_collect_dir(config)
    record_fname = path.join(base_dir, "records.json")
    with open(record_fname) as f:
        records = json.load(f)
    return records


def save_scene_polygons(
        config, ground_truth=True, shell_color='green', hole_color='orange', circle_color='blue', 
        star_color='gold', plot_lidar_points=True, lidar_color='purple', **kwargs):
    """Save pictures pictures of the scene for each uid and its associated landing site polygon

    Arguments:
        config {[type]} -- Configuration parameters

    Keyword Arguments:
        ground_truth {bool} -- Whether to use the ground truth segmentation (default: {True})
        shell_color {str} -- Color of the outer hull for the landing polygon (default: {'green'})
        hole_color {str} -- Color of the hole color (default: {'orange'})
        circle_color {str} -- Color of the greatest inscribed (default: {'blue'})
        star_color {str} -- Color of the star (default: {'gold'})

    Returns:
        [] - Empty lis, no stat records saved
    """

    stat_records = []
    for record in config['records']:
        try:
            uid = record['uid']
            building = record['label']
            environment = config['environment']
            command = 'scenepolygon'
            tag = 'groundtruth' if ground_truth else 'predicted'

            img_meta = record['imgs'][0]
            img_meta['rotation'] = Quaternionr(**img_meta['rotation'])
            img_meta['position'] = Vector3r(**img_meta['position'])
            img = load_image(config, uid, 'Scene')
            classified_point_cloud = load_point_cloud(config, uid, classified=True, ground_truth=ground_truth)
            point_pixels = project_ned_points(classified_point_cloud[:,:3], img_meta)
            # print(point_pixels)
            fig, ax = plt.subplots(1)
            plt.imshow(img)
            ax.set_title("UID: {}, Building: {}".format(uid, building))

            poly, height = load_polygon(config, uid, ground_truth=ground_truth)
            if poly:
                ms, (point, radius) = extract_largest_circle(poly, **kwargs)
                circle = Point(point).buffer(radius)

                if plot_lidar_points:
                    ax.scatter(point_pixels[:, 0], point_pixels[:, 1], color=lidar_color, s=0.5)

                poly_pixel = polygon_to_pixel_coords(poly, height, img_meta)
                circle_pixel = polygon_to_pixel_coords(circle, height, img_meta)
                point_pixel = transform_points_to_pixel_coords(
                    [[circle.centroid.x, circle.centroid.y]], height, img_meta)[0, :]

                # TODO should I show the buffered polygon (reduced) or original
                plot_polygon(poly_pixel, ax, shell_color=shell_color, hole_color=hole_color)
                plot_polygon(circle_pixel, ax, shell_color=circle_color)
                ax.scatter(point_pixel[0], point_pixel[1], marker='*', color=star_color)

            img_file_name = get_img_file_name(config, uid, img_type="ScenePolygon", ground_truth=ground_truth)
            # plt.show()
            fig.savefig(img_file_name, bbox_inches='tight')
        except Exception as e:
            logging.warn("Error! Skipping uid {} for command save_scene_polygon".format(uid))
    return stat_records


def create_new_dirs(config):
    """Ensured directories that will be written to are created"""
    base_dir = get_base_collect_dir(config)
    makedirs(path.join(base_dir, "LidarClassified"), exist_ok=True)
    makedirs(path.join(base_dir, "Polygons"), exist_ok=True)
    makedirs(path.join(base_dir, "SegmentationPredicted"), exist_ok=True)
    makedirs(path.join(base_dir, "ScenePolygon"), exist_ok=True)


def prepare_config(config):
    """Prepares the configuration file"""
    config['records'] = read_records(config)
    # append_building_name_to_records(config)
    create_new_dirs(config)
    # Get seg2rgb map
    config['num_classes'] = len(set([code[1] for code in config['collect_config']['segmentation_codes']]))
    _, seg2rgb_map = get_seg2rgb_map(config['num_classes'], config['collect_config']['color_codes'], normalized=False)
    config['seg2rgb_map'] = seg2rgb_map


def save_stat_records(config, stat_records, fname="stat_records.json"):
    base_dir = get_base_collect_dir(config)
    stat_records_fname = path.join(base_dir, fname)
    stat_records_dict = [stat_record._asdict() for stat_record in stat_records]
    with open(stat_records_fname, 'w') as file:
        json.dump(stat_records_dict, file, indent=2)


def execute_commands(config):
    """Executes commands given"""
    stat_records = []
    for command in config['commands']:
        try:
            if not command.get('active', True):
                continue
            logging.info("Starting Command %r", command['command'])
            if command['command'] == "classify_point_clouds":
                stat_records.extend(classify_points_clouds(config, **command['kwargs']))
            if command['command'] == "polylidar":
                polylidar_kwargs = config['polylidar_kwargs']
                stat_records.extend(extract_polygons(config, polylidar_kwargs=polylidar_kwargs, **command['kwargs']))
            if command['command'] == "polylabel":
                stat_records.extend(extract_largest_circles(config, **command['kwargs']))
            if command['command'] == "scenepolygon":
                stat_records.extend(save_scene_polygons(config, **command['kwargs']))
            logging.info("Finished Command %r", command['command'])
        except Exception:
            logging.warn("Issue, skipping command {}".format(command))
            traceback.print_exc(file=sys.stdout)
        # skip inactive commands
    return stat_records


def main():
    parser = argparse.ArgumentParser(description='Gather statistical data for rooftop landing paper')
    parser.add_argument("--config", "-c", type=str, required=True,
                        help="Path to configuration file")
    parser.add_argument("--output", "-o", type=str, required=False, default="stat_records.json",
                        help="Path to output file")

    args = parser.parse_args()

    # Load config file
    with open(args.config) as f:
        main_config = json.load(f)
    # Load collector config file
    collector_config_fname = main_config['collect_config']
    with open(collector_config_fname) as f:
        collect_config = json.load(f)

    # load geojson information
    main_config['collect_config'] = collect_config

    # Match every record to a building name
    prepare_config(main_config)
    stat_records = execute_commands(main_config)
    save_stat_records(main_config, stat_records, args.output)


if __name__ == "__main__":
    main()
