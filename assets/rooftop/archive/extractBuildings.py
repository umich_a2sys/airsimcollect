## SCRIPT DOES NOT WORK ON WINDOWS
import subprocess
from os import path

dest_dir = 'buildings'

buildings =  ['building6_example2', 'building6_example', 'Building2_Example3', 'Building3_Example']
files_per_building = 4

static_args = "-csv --csv-header X,Y,Z,C -s 1,1,-1,1"

total_files = 16

for i in range(0, total_files, files_per_building):
    buildingIndex = int((i + 1) / (files_per_building))
    # print(buildingIndex)
    file_range = "-f [{}-{}]-0.npy".format(i, i + files_per_building - 1)
    file_single = "-f {}-0.npy".format(i)

    outfile = path.join(dest_dir, buildings[buildingIndex])
    range_path = outfile + "_360.csv"
    single_path = outfile + "_90.csv"

    argsRange = "cm npjoin {} {} -o {}".format(file_range, static_args, range_path)
    argsSingle = "cm npjoin {} {} -o {}".format(file_single, static_args, single_path)
    print(argsRange)
    p = subprocess.Popen(argsRange, stdout=subprocess.PIPE, shell=True)
    print(p.communicate())
    print()
    print(argsSingle)
    p = subprocess.Popen(argsSingle, stdout=subprocess.PIPE, shell=True)
    print(p.communicate())

    

# subprocess.run()  # doesn't capture output


files = []