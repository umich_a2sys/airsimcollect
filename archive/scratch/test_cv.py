
import cv2
import numpy as np


criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((6*7,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

print(objp.shape)
print(objp.dtype)


criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
img = cv2.imread(r"C:\Users\Jerem\Downloads\calib_result.jpg")
# print(img)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, corners = cv2.findChessboardCorners(gray, (7,6), None)
corners2=cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
print(ret)
# print(corners2)
print(corners2.shape)
print(corners2.dtype)


ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera([objp], [corners2], gray.shape[::-1],None,None)