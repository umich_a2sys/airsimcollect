
import logging
import numpy as np
import math

import matplotlib.pyplot as plt
import cv2

import airsim
from airsim import Vector3r, Pose, to_quaternion, ImageRequest
from airsim.types import ImageType

from cameracalibration.helpers import transform_ue4_to_cam, unreal_to_ned


DEBUG = False
SHOW_REPROJECTIONS = True

logging.basicConfig()
LOGGER = logging.getLogger('AirSimCameraCal')
LOGGER.setLevel(logging.INFO)

np.set_printoptions(suppress=True, precision=2, linewidth=120)


# SPHERE COLOR RANGES FOR OBJECT DETECTION
HSV_RANGES = {
    'green': [np.array([55,140,140]), np.array([65,255,255])],
    'red': [np.array([0,140,140]), np.array([20,255,255])],
    'blue': [np.array([110,150,150]), np.array([130,255,255])]
}

SPHERE_RADIUS = 25 # centimeters, unreal units


# UNREAL ENGINE COORDINATES
START_X = 1500
START_Y = -3000
START_Z = 400

X_SPACING = 500
Y_SPACING = -200
Z_SPACING = 200

N_X = 3
N_Y = 4
N_Z = 4

Z_HEIGHTS = list(range(START_Z, START_Z + Z_SPACING * N_Z, Z_SPACING))

# Objectlist starts with green sphere (sphere with lowest x axis). The goes up in the z direction
# Then moves to the red sphere line and goes up (z direction)
# Then moves to the blue spheres

OBJ_POINTS_UNREAL = []
for x in range(START_X, START_X + X_SPACING*N_X , X_SPACING):
    for i, y in enumerate((list(range(START_Y, START_Y + Y_SPACING*(N_Y -1) , Y_SPACING)) + [START_Y])):
        # y has this offset where the 4th sphere (at the top) circles back to the first level
        # z height is the same for the same y-value
        z = Z_HEIGHTS[i]
        OBJ_POINTS_UNREAL.append([x,y,z])

OBJ_POINTS_UNREAL = np.array(OBJ_POINTS_UNREAL)


class CalibrateCamera(object):
    def __init__(self):
        self.client = airsim.MultirotorClient()
        self.client.confirmConnection()


    def get_picture(self):
        img_req = ImageRequest('0',ImageType.Scene, False, False)
        image_responses = self.client.simGetImages([img_req])
        img_rgba = None
        camera_position = None
        camera_orientation = None
        for _, response in enumerate(image_responses):
            width, height, camera_position, camera_orientation = response.width, response.height, response.camera_position, response.camera_orientation
            img1d = np.fromstring(response.image_data_uint8, dtype=np.uint8) #get numpy array
            img_rgba = img1d.reshape(height, width, 4) #reshape array to 4 channel image array H X W X 4
        return img_rgba, camera_position, camera_orientation

    def createBlobDetector(self):
        params = cv2.SimpleBlobDetector_Params()
        # Change thresholds
        params.minThreshold = 0    # the graylevel of images
        params.maxThreshold = 255
        params.filterByColor = True
        params.blobColor = 0
        params.minArea = 1
        params.filterByInertia = False
        params.filterByConvexity = False
        detector = cv2.SimpleBlobDetector.create(params)
        return detector


    def capturePoints(self):
        # Capture airsim picture and drop alpha channel
        img, cam_pos, cam_ori = self.get_picture()
        img = img[:,:, :3]
        height, width, _ = img.shape

        detector = self.createBlobDetector()

        img_points = []
        # convert RGB numpy array to open cv hsv colorspace
        cv_img = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
        mask_all = np.ones((height, width), np.uint8) * 255
        for color_name, ranges in HSV_RANGES.items():
            mask = cv2.bitwise_not(cv2.inRange(cv_img, *ranges))
            mask_all = mask_all & mask
            keypoints = detector.detect(mask)
            color_points = []
            if keypoints:
                LOGGER.debug("%s Circles", color_name)
                for keyPoint in keypoints:
                    x = int(round(keyPoint.pt[0]))
                    y = int(round(keyPoint.pt[1]))
                    s = keyPoint.size
                    LOGGER.debug("x: %d, y: %d", x, y)
                    color_points.append([x, y])
                color_points.sort(key=lambda x: x[1], reverse=True)
                img_points.extend(color_points)
            else:
                LOGGER.warn("No %s circles detected! Do you see them in the image?", color_name)
                continue
        LOGGER.debug("Image points %r", img_points)

        if DEBUG:
            fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(16, 8))
            ax1.imshow(img)
            ax2.imshow(mask_all)
            plt.show()

        obj_points = transform_ue4_to_cam(OBJ_POINTS_UNREAL, cam_pos, cam_ori)
        # Transpose and remove the one
        obj_points = obj_points.transpose()[:,:3].astype(np.float32)
        img_points = np.expand_dims(np.array(img_points),1).astype(np.float32)
        return obj_points, img_points, (height, width)


    def project_points(self, points, camera_matrix):
        temp = points.copy()
        points = np.ones(shape=(4, points.shape[0]))
        points[:3, :] = temp.transpose()

        proj_points = camera_matrix.dot(points)
        pixels = np.divide(proj_points[:2, :], proj_points[2, :]).transpose().astype(np.int)
        return pixels

    def calibrateFromPositions(self):
        positions = [
                        [2000, -1500, 400, 0, 0, math.radians(-90)],
                        [2000, -1200, 400, 0, 0, math.radians(-90)],
                        [2200, -1500, 400, 0, 0, math.radians(-90)],
                        [1800, -1200, 400, 0, 0, math.radians(-90)],
                        [2000, -2000, 400, 0, 0, math.radians(-90)]
                    ]

        img_points =[]
        obj_points = []
        img_shape = None
        for pos in positions:
            pos, rot = unreal_to_ned(*pos)
            self.client.simSetVehiclePose(Pose(pos, rot), True)
            obj_points_, img_points_, img_shape = self.capturePoints()
            obj_points.append(obj_points_)
            img_points.append(img_points_)

        # This can be omitted and can be substituted with None below.
        camera_matrix = np.array([[256, 0, 256], 
                                [0, 256, 256], 
                                [0, 0, 1]])
        # camera_matrix = cv2.initCameraMatrix2D(obj_points,img_points, img_shape)
        LOGGER.info("Estimated Camera Matrix: \n %r", camera_matrix)
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, img_shape, camera_matrix, None, flags=cv2.CALIB_USE_INTRINSIC_GUESS)
        LOGGER.info("Calibrated Camera Matrix: \n %r", mtx)
        LOGGER.info("Distortion Values: %r", dist)
        LOGGER.info("Reprojection error from %d images: %.2f", len(img_points), ret)

        proj_mat = np.zeros((3,4))
        proj_mat[:3,:3] = camera_matrix


        if SHOW_REPROJECTIONS:
            for obj_points_, img_points_ in zip(obj_points, img_points):
                img_points_ = np.squeeze(img_points_)
                proj_points = self.project_points(obj_points_, proj_mat)
                # print(proj_points)
                # print(img_points_)
                resid = proj_points - img_points_
                print(resid)






def main():
    LOGGER.debug("Object Points in Unreal Coordinates: \n %r", OBJ_POINTS_UNREAL)
    cal_cam = CalibrateCamera()
    cal_cam.calibrateFromPositions()

if __name__ == "__main__":
    main()