import numpy as np
import quaternion
import math

from airsim import Vector3r, Pose, to_quaternion, ImageRequest

def transform_ue4_to_cam(points, cam_pos, cam_ori, points_in_unreal=True):
    temp = points.copy()
    points = np.ones(shape=(4, points.shape[0]))
    points[:3, :] = temp.transpose()

    if points_in_unreal:
        # Need to scale down to meters
        points[:3, :] = points[:3, :] / 100.0
        # Need to convert to NED coordinate for homogoneous transformation matrix
        temp = points.copy()
        points[0, :], points[1, :], points[2, :] = temp[0,:], temp[1,:], -temp[2,:] 
    else:
        pass
    
    # print(points)
    # print(cam_pos)
    # print(cam_ori)
    

    # Points are in NED, wide form
    # Now transform them 
    hom_transform = create_homogenous_transform(cam_pos, cam_ori)

    point_cam_ned = hom_transform.dot(points)
    # print(point_cam_ned)
    point_cam_hom = point_cam_ned.copy()
    point_cam_hom[0, :], point_cam_hom[1, :], point_cam_hom[2, :] = point_cam_ned[1,:], point_cam_ned[2,:], point_cam_ned[0,:] 
    # print(point_cam_hom)
    return point_cam_hom

def create_homogenous_transform(cam_pos, rot):
    inv_rot_q = np.quaternion(rot.w_val, - rot.x_val, - rot.y_val, - rot.z_val)
    cam_pos = np.array([cam_pos.x_val, cam_pos.y_val, cam_pos.z_val])

    inv_rot_mat = quaternion.as_rotation_matrix(inv_rot_q)

    hom_tran = np.zeros(shape=(4,4))
    hom_tran[:3, :3] = inv_rot_mat
    hom_tran[:3, 3] = -1 * inv_rot_mat.dot(cam_pos)
    hom_tran[3,3] = 1

    return hom_tran

def unreal_to_ned(x, y, z, pitch, roll, yaw):
    pos = Vector3r(x / 100, y / 100, -z / 100)
    rot = to_quaternion(pitch, roll, yaw)
    return pos, rot

