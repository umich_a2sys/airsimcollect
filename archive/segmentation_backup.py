"""Segmentation Module for airsim

"""
import logging

logger = logging.getLogger("AirSim Capture")
logger.setLevel(logging.DEBUG)

client = airsim.VehicleClient()
client.confirmConnection()

# airsim.wait_key('Press any key to set all object IDs to 0')
# found = client.simSetSegmentationObjectID("[\w*. ]*", 0, True);
# print("Done: %r" % (found))

def set_segmentation_ids(client, regex_codes):
    pass

airsim.wait_key('Press any key to set all buildings to one color')
found = client.simSetSegmentationObjectID("Building.*[.].*", 10, True)
print("Done: %r" % (found))
airsim.wait_key('Press any key to set rooftop color')
found = client.simSetSegmentationObjectID("Building.*[.]BLD_R.*", 20, True)
print("Done: %r" % (found))

#get segmentation image in various formats
responses = client.simGetImages([
    # airsim.ImageRequest("0", airsim.ImageType.Segmentation, True), #depth in perspective projection
    airsim.ImageRequest("0", airsim.ImageType.Segmentation, False, False)])  #scene vision image in uncompressed RGBA array
print('Retrieved images: %d', len(responses))

#save segmentation images in various formats
for idx, response in enumerate(responses):
    filename = 'c:/temp/py_seg_' + str(idx)

    if response.pixels_as_float:
        print("Type %d, size %d" % (response.image_type, len(response.image_data_float)))
        airsim.write_pfm(os.path.normpath(filename + '.pfm'), airsim.get_pfm_array(response))
    elif response.compress: #png format
        print("Type %d, size %d" % (response.image_type, len(response.image_data_uint8)))
        airsim.write_file(os.path.normpath(filename + '.png'), response.image_data_uint8)
    else: #uncompressed array - numpy demo
        print("Type %d, size %d" % (response.image_type, len(response.image_data_uint8)))
        img1d = np.fromstring(response.image_data_uint8, dtype=np.uint8) #get numpy array
        img_rgba = img1d.reshape(response.height, response.width, 4) #reshape array to 4 channel image array H X W X 4
        img_rgba = np.flipud(img_rgba) #original image is flipped vertically
        airsim.write_png(os.path.normpath(filename + '.numpy.png'), img_rgba) #write to png 



    # template<class T>
    # static std::string GetMeshName(T* mesh)
    # {
	# 	std::regex name_regex;
	# 	name_regex.assign("Building.*", std::regex_constants::icase);
    #     switch (mesh_naming_method_)
    #     {
    #     case msr::airlib::AirSimSettings::SegmentationSetting::MeshNamingMethodType::OwnerName:
	# 		if (mesh->GetOwner())
	# 		{
    #             auto name = std::string(TCHAR_TO_UTF8(*(mesh->GetOwner()->GetName())));
	# 			if (std::regex_match(name, name_regex))
	# 			{
	# 				auto name1 = std::string(TCHAR_TO_UTF8(*(UKismetSystemLibrary::GetDisplayName(mesh))));
	# 				return name1;
	# 			}
	# 			else
	# 			{
	# 				return name;
	# 			}

	# 		}
    #         else
    #             return ""; //std::string(TCHAR_TO_UTF8(*(UKismetSystemLibrary::GetDisplayName(mesh))));
    #     case msr::airlib::AirSimSettings::SegmentationSetting::MeshNamingMethodType::StaticMeshName:
	# 		if (mesh->GetStaticMesh())
    #             return std::string(TCHAR_TO_UTF8(*(mesh->GetStaticMesh()->GetName())));
    #         else
    #             return "";
    #     default:
    #         return "";
    #     }
    # }
