"""Visualized LiDAR Data

"""
import argparse
import time
from itertools import count
import logging
from multiprocessing import Process, Queue
import warnings

# ignore quaternions warning about numba not being installed
# ignore vispy warning about matplotlib 2.2+ issues
warnings.simplefilter("ignore")

import numpy as np

from vispy import app
import vispy.scene
from vispy.scene import visuals
from vispy.io import read_png

import airsim
from airsim.types import ImageType, ImageRequest

from lidarsegmentation.helpers import (
    height2rgb, create_homogenous_transform, create_projection_matrix, transform_to_cam, project_points_img,
    get_colors_from_image, set_segmentation_ids, parse_lidarData, same_pose, get_segmentation_codes)


LOGGER = logging.getLogger('AirSimVis')
LOGGER.setLevel(logging.INFO)

# Time to update vispy graphics and gather sensor data (seconds)
DEFAULT_VISPY_TIMER_INTERVAL = 1 / 60
DEFAULT_AIRSIM_TIMER_INTERVAL = 1 / 10

DEFAULT_WINDOW_SIZE = (1536, 1024)
IMG_SIZE = DEFAULT_WINDOW_SIZE[1] // 2
DEFAULT_PROJECTED_POINTS = (255, 0, 0, 1)


np.set_printoptions(suppress=True, precision=3)


class AirSimCollector():
    def __init__(self, async_q, color_mode='height', segmentation_codes=[], imgs=['scene', 'segmentation']):
        self.async_q = async_q
        self.color_mode = color_mode
        self.imgs = imgs
        self.segmentation_codes = segmentation_codes

        self.client = airsim.MultirotorClient()
        self.client.confirmConnection()

        # color mapper
        set_segmentation_ids(self.client, self.segmentation_codes)
        self.cmapper = height2rgb()


    def get_image_requests(self):
        image_requests = []
        if 'scene' in self.imgs:
            image_requests.append(ImageRequest("0", ImageType.Scene, False, False))
        if 'segmentation' in self.imgs:
            image_requests.append(ImageRequest("0", ImageType.Segmentation, False, False))
        return image_requests

    def collect_data(self):
        old_pose = None
        while True:
            time.sleep(DEFAULT_AIRSIM_TIMER_INTERVAL)
            pose = self.client.simGetVehiclePose()
            if same_pose(old_pose, pose.position):
                continue
            old_pose = pose.position

            # self.client.simPause(True)
            # Fetch data from AirSim
            camera_info = self.client.simGetCameraInfo(0)
            image_requests = self.get_image_requests()
            image_responses = self.client.simGetImages(image_requests)
            lidar_data = self.client.getLidarData()
            # self.client.simPause(False)

            width = None
            height = None
            commands = []
            # keep a reference to the segmentation image
            seg_img = None
            # Fetch images
            for i, response in enumerate(image_responses):
                width, height = response.width, response.height
                img1d = np.fromstring(response.image_data_uint8, dtype=np.uint8)  # get numpy array
                if img1d.size < 2:
                    LOGGER.warn("Image returned empty")
                    continue
                # reshape array to 4 channel image array H X W X 4
                img_rgba = img1d.reshape(response.height, response.width, 4)
                if (response.image_type == ImageType.Segmentation):
                    seg_img = img_rgba
                    LOGGER.debug("Camera %r pos: %r; ", response.image_type, response.camera_position)
                    LOGGER.debug("Camera %r orientation: %r; ",  response.image_type, response.camera_orientation)

                # img_rgba = np.flipud(img_rgba) #original image is flipped vertically
                commands.append({'data': img_rgba, 'type': 'image', 'id': response.image_type})

            LOGGER.debug("Lidar position: %r; ", lidar_data.pose.position)
            if (len(lidar_data.point_cloud) < 3):
                LOGGER.debug("No lidar points received")
            else:
                points = parse_lidarData(lidar_data)
                # Project points into segmentation image if available
                if seg_img is not None:
                    # Transform and project point cloud into segmentation image
                    cam_ori = camera_info.pose.orientation
                    cam_pos = camera_info.pose.position
                    proj_mat = create_projection_matrix(height, width)
                    # Transform NED points to camera coordinate system (not NED)
                    points_transformed = transform_to_cam(points, cam_pos, cam_ori, points_in_unreal=False)
                    # Project Points into image, filter points outside of image
                    pixels, points = project_points_img(points_transformed, proj_mat, width, height, points)
                    # Ensure we have valid points
                    if points.shape[0] < 1:
                        continue

                color = None
                if self.color_mode == 'height':
                    color = self.cmapper(points[:, 2])
                elif self.color_mode == 'segmentation' and seg_img is not None:
                    color = get_colors_from_image(pixels, seg_img)
                LOGGER.debug("# Points: %r", points.shape)
                commands.append({'data': points, 'type': 'points', 'id': None, 'color': color})

                # commands.append({'data': pixels, 'type': 'projected_points',
                #                  'id': ImageType.Scene, 'color': DEFAULT_PROJECTED_POINTS})

            self.async_q.put(commands)


class AirSimVisualizer(object):

    def __init__(self, sync_q, point_size=5, face_color=(1, 1, 1, .5), persist_mode=True):
        # The synchronous queue needed to get command data
        self.sync_q = sync_q
        self.canvas = vispy.scene.SceneCanvas(size=DEFAULT_WINDOW_SIZE, keys='interactive', show=True)
        self.grid = self.canvas.central_widget.add_grid(margin=0)

        self.point_view = self.grid.add_view(0, 0, row_span=2, col_span=2)
        self.point_view.camera = vispy.scene.cameras.TurntableCamera(fov=45, distance=20, flip=(False, False, True))
        self.axis = visuals.XYZAxis(parent=self.point_view.scene)

        self.img_view_rgb = self.grid.add_view(0, 2)
        img_data = np.zeros((IMG_SIZE, IMG_SIZE, 4), dtype=np.uint8)

        self.img_rgb = visuals.Image(img_data, interpolation='nearest',
                                     parent=self.img_view_rgb)

        self.img_view_seg = self.grid.add_view(1, 2)
        img_data = np.zeros((IMG_SIZE, IMG_SIZE, 4), dtype=np.uint8)
        self.img_seg = visuals.Image(img_data, interpolation='nearest',
                                     parent=self.img_view_seg)

        self.img_ids = {ImageType.Scene: self.img_rgb, ImageType.Segmentation: self.img_seg}

        self.timer = app.Timer(DEFAULT_VISPY_TIMER_INTERVAL, connect=self.on_timer, start=True)
        # TODO not currently used
        # Have multiple markers with ids that can be updated
        self.markers = {}
        self.counter = count()

        self.point_size = point_size
        self.face_color = face_color
        self.persist_mode = persist_mode
        self.total_points = 0
        self.markers = visuals.Markers()
        self.point_view.add(self.markers)
        self.points = np.array([[0,0,0]], dtype=np.float32)
        self.colors = np.array([self.face_color], np.float32)
        

    def add_points(self, points, key=None):
        pass

    def create_or_update_points(self, command):
        """Creates or updates the vispy markers in the scene

        Arguments:
            command {dict} -- A command holds {'type': ['update' or 'create'],  'points': ndarray, id: int, 'color': ndarray or tuple}
        """

        id_ = command.get('id')
        points = command.get('data')
        if points.shape[0] < 1:
            return
        face_color = command.get('color') if command.get('color') is not None else self.face_color
        if id_ is None:
            marker = self.markers
            if self.persist_mode:
                self.points = np.vstack((self.points, points))
                self.colors = face_color if isinstance(face_color, tuple) else np.vstack((self.colors, face_color))

            else:
                self.points = points
                self.colors = face_color
            self.total_points = self.points.shape[0]
        else:
            marker = self.markers.get(id_)
            raise NotImplementedError("Have not implemented id marker updates")


        try:
            if marker:
                LOGGER.debug("Total Points %r", self.total_points)
                marker.set_data(self.points[:,:3], edge_color=None, face_color=self.colors, size=self.point_size)
            else:
                LOGGER.warn("No such marker to update %r", id_)
        except Exception as e:
            LOGGER.exception("Error updating marker")

    def update_image(self, command):
        """Creates or updates the vispy markers in the scene

        Arguments:
            command {dict} -- A command holds {'type': ['update' or 'create'],  'points': ndarray, id: int, 'color': ndarray or tuple}
        """

        img_id = command.get('id')
        img_data = command.get('data')

        img = self.img_ids[img_id]
        img.set_data(img_data)

    def project_points(self, command):
        """Creates or updates the vispy markers in the scene

        Arguments:
            command {dict} -- A command holds {'type': ['update' or 'create'],  'points': ndarray, id: int, 'color': ndarray or tuple}
        """

        img_id = command.get('id')
        img_indices = command.get('data')
        # print(img_indices)
        # print("Projected points command!")

        img = self.img_ids[img_id]
        data = img._data

        data[img_indices[:, 1], img_indices[:, 0]] = [255, 0, 0, 255]
        img.set_data(data)

    def on_timer(self, event):
        """Timer that looks for new data from airsim collector thread

        Arguments:
            event {} -- vispy event
        """

        while not self.sync_q.empty():
            # We have data
            commands = self.sync_q.get()
            for command in commands:
                try:
                    LOGGER.debug('New command received %r', command)
                    if command.get('type') in ['points']:
                        self.create_or_update_points(command)
                    elif command.get('type') == 'image':
                        self.update_image(command)
                    elif command.get('type') == 'projected_points':
                        self.project_points(command)
                except Exception as _:
                    LOGGER.exception("Error in command")
                    LOGGER.error(command)


def airsim_coroutine(async_q, color_mode='height', segmentation_codes=[], imgs=['scene', 'segmentation']):
    """Airsim coroutine that will gather data and send data as commands through queue

    Arguments:
        async_q {Queue} -- Command with data queue

    Keyword Arguments:
        color_mode {str} -- How should the points be colored (default: {'height'})
    """

    LOGGER.info("Running Async Airsim Coroutine")
    asc = AirSimCollector(async_q, color_mode=color_mode, segmentation_codes=segmentation_codes, imgs=imgs)
    asc.collect_data()


def vispySynchronousWindow(sync_q, persist_mode=True):
    """Tthread that run the VisPy window

    Arguments:
        sync_q {Queue} -- Queue that that the synchronous vispy will push to

    Keyword Arguments:
        persist_mode {bool} -- Should all points created persist (default: {True})
    """
    try:
        pv = AirSimVisualizer(sync_q, persist_mode=persist_mode)
    except Exception as e:
        LOGGER.exception("Error starting Vispy window")
    app.run()


def main():
    parser = argparse.ArgumentParser(description='Visualize AirSim PointClouds and Images')
    parser.add_argument("-pm", "--persist_mode", action='store_true', default=False,
                        help="Persist lidar points gathered in VisPy")
    parser.add_argument("-cm", "--color_mode", choices=['height', 'segmentation'], default='height',
                        help="Color of points"),
    parser.add_argument('-i','--imgs', nargs='+', required=False, 
                        help='Images to gather: scene, segmentation', default=['scene', 'segmentation'])
    parser.add_argument("-c", "--config", help="Config file with segmentation ids")
    args = parser.parse_args()

    if args.config:
        seg_codes = get_segmentation_codes(args.config)
    else:
        seg_codes = []

    queue = Queue()
    vispy_process = Process(target=vispySynchronousWindow, args=(queue, args.persist_mode))
    vispy_process.start()
    airsim_coroutine(queue, color_mode=args.color_mode, segmentation_codes=seg_codes, imgs=args.imgs)
    vispy_process.join()


if __name__ == "__main__":
    main()
